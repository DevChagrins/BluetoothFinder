#include "NinBlueComms.h"

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <conio.h>
#include <iostream>
#include <initguid.h>
#include <windef.h>
//#include <tchar.h>
using namespace std;


NinBlueComms::NinBlueComms()
{
}


NinBlueComms::~NinBlueComms()
{
}

bool NinBlueComms::FindBtDev(BLUETOOTH_DEVICE_INFO* desired_device_info, wchar_t* device_name)
{
    //======================Handles=======================================================
    HBLUETOOTH_RADIO_FIND tBtRadioFind;
    HBLUETOOTH_DEVICE_FIND tBtDeviceFind;

    int m_radio_id;
    int m_device_id;
    DWORD mbtinfo_ret;
    bool FuncResult = false;

    BLUETOOTH_FIND_RADIO_PARAMS tBtFindRadio = { sizeof(BLUETOOTH_FIND_RADIO_PARAMS) };
    BLUETOOTH_RADIO_INFO tBtInfo = { sizeof(BLUETOOTH_RADIO_INFO),0, };
    BLUETOOTH_DEVICE_SEARCH_PARAMS tSearchParams =
    {
        sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS),
        1,
        0,
        1,
        1,
        1,
        15,
        NULL
    };
    BLUETOOTH_DEVICE_INFO tDeviceInfo = { sizeof(BLUETOOTH_DEVICE_INFO),0, };

    //====================================================================================
    // Iterate for available bluetooth radio devices in range
    // Starting from the local
    tBtRadioFind = BluetoothFindFirstRadio(&tBtFindRadio, &mRadio);
    if (tBtRadioFind == NULL)
    {
        cout << "Bluetooth Radio failed with error code" << GetLastError() << endl;
        FuncResult = false;
    }

    m_radio_id = 0;

    do
    {
        // Then get the radio device info....
        mbtinfo_ret = BluetoothGetRadioInfo(mRadio, &tBtInfo);

        if (mbtinfo_ret != ERROR_SUCCESS)
        {
            cout << "BluetoothGetRadioInfo() failed wit error code " << mbtinfo_ret << endl;
            FuncResult = false;
        }

        tSearchParams.hRadio = mRadio;
        ZeroMemory(&tDeviceInfo, sizeof(BLUETOOTH_DEVICE_INFO));
        tDeviceInfo.dwSize = sizeof(BLUETOOTH_DEVICE_INFO);

        // Next for every radio, get the device
        tBtDeviceFind = BluetoothFindFirstDevice(&tSearchParams, &tDeviceInfo);

        if (tBtDeviceFind == NULL)
        {
            cout << "Failed to find Devices with error code:  " << GetLastError() << endl;
            FuncResult = false;
        }
        else
        {
            m_radio_id++;
            m_device_id = 0;

            //Get the device info
            do
            {
                m_device_id++;

                if (wcsstr(tDeviceInfo.szName, device_name) != nullptr) //this search specifically for hc-05 bluetooth module.
                {
                    *desired_device_info = tDeviceInfo;
                    FuncResult = true;
                    break;
                }
                else
                {
                    FuncResult = false;
                }
            } while (BluetoothFindNextDevice(tBtDeviceFind, &tDeviceInfo));
        }

        // NO more device, close the device handle
        if (BluetoothFindDeviceClose(tBtDeviceFind) == FALSE)
        {
            FuncResult = false;
        }

    } while (BluetoothFindNextRadio(tBtRadioFind, &mRadio));

    // No more radio, close the radio handle
    if (BluetoothFindRadioClose(tBtRadioFind) == FALSE)
    {
        cout << "BluetoothFindRadioClose() failed with error code " << GetLastError() << endl;
        FuncResult = false;
    }

    return FuncResult;
}
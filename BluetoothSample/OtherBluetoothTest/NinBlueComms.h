#ifndef _NIN_BLUE_COMMS_
#define _NIN_BLUE_COMMS_

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "irprops.lib")
#pragma comment(lib, "Bthprops.lib")

#include <winsock2.h>
#include <windows.h>
#include <ws2bth.h>
#include <BluetoothAPIs.h>

/// Bluetooth states.
enum RadioMode //enumeration created for bluetooth radio state.
{
    /// Bluetooth off.
    Off,
    /// Bluetooth is on but not discoverable.
    On,
    /// Bluetooth is on and discoverable.
    Discoverable,
};

class NinBlueComms
{
private:
    HANDLE mRadio;

public:
    NinBlueComms();
    ~NinBlueComms();

    bool FindBtDev(BLUETOOTH_DEVICE_INFO* desired_device_info, wchar_t* device_name);
};

#endif // _NIN_BLUE_COMMS_
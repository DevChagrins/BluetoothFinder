#include "BthComms.h"

#include <iostream>
using namespace std;

#include <strsafe.h>
#include <intsafe.h>

//DEFINE_GUID(g_guidServiceClass, 0xb62c4e8d, 0x62cc, 0x404b, 0xbb, 0xbf, 0xbf, 0x3e, 0x3b, 0xbb, 0x13, 0x74);
#define CXN_INSTANCE_STRING L"Sample Bluetooth Server"
#define CXN_DEFAULT_LISTEN_BACKLOG 4
#define CXN_TRANSFER_DATA_LENGTH 512

// Pass in the GUID String
int BthComms::RunServer(GUID _GUID, LPWSTR _serviceName)
{
    int tErr = 0;
    int tAddrLen = sizeof(SOCKADDR_BTH);
    WSAQUERYSET tWSAQuerySet = {0};
    SOCKET tLocalSocket = INVALID_SOCKET;
    SOCKET tClientSocket = INVALID_SOCKET;
    size_t tInstanceNameSize = 0;
    DWORD tComputerNameLength = MAX_COMPUTERNAME_LENGTH + 1;
    wchar_t tComputerName[MAX_COMPUTERNAME_LENGTH + 1];
    wchar_t* tInstanceName = NULL;
    char* tDataBuffer = nullptr;
    char* tDataBufferIndex = nullptr;
    HRESULT tResult = NULL;

    tLocalSocket = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);

    if (tLocalSocket == INVALID_SOCKET)
    {
        cout << "Socket failed to create with error " << WSAGetLastError() << "\n";
        WSACleanup();
        return 1;
    }

    cout << "Socket made.\n";

    SOCKADDR_BTH tSockAddrBthLocal;
    tSockAddrBthLocal.addressFamily = AF_BTH;
    tSockAddrBthLocal.btAddr = 0;
    tSockAddrBthLocal.serviceClassId = GUID_NULL;
    tSockAddrBthLocal.port = BT_PORT_ANY;

    tErr = bind(tLocalSocket, (sockaddr*)&tSockAddrBthLocal, sizeof(tSockAddrBthLocal));

    if (tErr == SOCKET_ERROR)
    {
        cout << "Bind failed with error " << WSAGetLastError() << "\n";
        closesocket(tLocalSocket);
        WSACleanup();
        return 2;
    }

    cout << "Binding succeeded.\n";

    tErr = getsockname(tLocalSocket, (sockaddr*)&tSockAddrBthLocal, &tAddrLen);

    if (tErr == SOCKET_ERROR)
    {
        cout << "getsockname() call failed " << WSAGetLastError() << "\n";
        closesocket(tLocalSocket);
        WSACleanup();
        return 3;
    }

    cout << "Socket name was gotten.\n";

    LPCSADDR_INFO lpCSAddrInfo = NULL;
    lpCSAddrInfo = (LPCSADDR_INFO)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(CSADDR_INFO));

    // Fill out address info
    lpCSAddrInfo[0].LocalAddr.iSockaddrLength = sizeof(SOCKADDR_BTH);
    lpCSAddrInfo[0].LocalAddr.lpSockaddr = (LPSOCKADDR)&tSockAddrBthLocal;
    lpCSAddrInfo[0].RemoteAddr.iSockaddrLength = sizeof(SOCKADDR_BTH);
    lpCSAddrInfo[0].RemoteAddr.lpSockaddr = (LPSOCKADDR)&tSockAddrBthLocal;
    lpCSAddrInfo[0].iSocketType = SOCK_STREAM;
    lpCSAddrInfo[0].iProtocol = BTHPROTO_RFCOMM;

    // Setup WSA Query Set
    ZeroMemory(&tWSAQuerySet, sizeof(WSAQUERYSET));
    tWSAQuerySet.dwSize = sizeof(WSAQUERYSET);
    tWSAQuerySet.lpServiceClassId = (LPGUID)&_GUID;

    // Get the computer name
    if (!GetComputerName(tComputerName, &tComputerNameLength))
    {
        cout << "=CRITICAL= | GetComputerName() call failed. WSAGetLastError=" << WSAGetLastError() << "\n";
        return 4;
    }

    tResult = StringCchLength(tComputerName, sizeof(tComputerName), &tInstanceNameSize);
    if (FAILED(tResult))
    {
        cout << "-FATAL- | ComputerName specified is too large\n";
        return 5;
    }


    tInstanceNameSize += sizeof(CXN_INSTANCE_STRING) + 1;
    tInstanceName = (LPWSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, tInstanceNameSize);
    if (NULL == tInstanceName) {
        cout << "-FATAL- | HeapAlloc failed | out of memory | gle = " << GetLastError() << "\n";
        return 6;
    }

    StringCbPrintf(tInstanceName, tInstanceNameSize, L"%s %s", tComputerName, CXN_INSTANCE_STRING);
    tWSAQuerySet.lpszServiceInstanceName = tInstanceName;//_serviceName;//tInstanceName;
    tWSAQuerySet.lpszComment = L"Example Service instance registered in the directory service through RnR";
    tWSAQuerySet.dwNameSpace = NS_BTH;
    tWSAQuerySet.dwNumberOfCsAddrs = 1;
    tWSAQuerySet.lpcsaBuffer = lpCSAddrInfo;

    // Changes to non-blocking accept and will advertise the service after accept has been called.
    if (SOCKET_ERROR == WSASetService(&tWSAQuerySet, RNRSERVICE_REGISTER, 0))
    {
        cout << "=CRITICAL= | WSASetService() call failed. WSAGetLastError=" << WSAGetLastError() << "\n";
        return 7;
    }

    if (SOCKET_ERROR == listen(tLocalSocket, CXN_DEFAULT_LISTEN_BACKLOG))
    {
        wprintf(L"=CRITICAL= | listen() call failed w/socket = [0x%I64X]. WSAGetLastError=[%d]\n", (ULONG64)tLocalSocket, WSAGetLastError());
        return 8;
    }

    cout << "Finally start accepting!" << endl;
    // Finally start accepting!
    for (int tCount = 0; (0 == tErr) && (tCount < 10); tCount++)
    {
        cout << "\n";

        //
        // accept() call indicates winsock2 to wait for any
        // incoming connection request from a remote socket.
        // If there are already some connection requests on the queue,
        // then accept() extracts the first request and creates a new socket and
        // returns the handle to this newly created socket. This newly created
        // socket represents the actual connection that connects the two sockets.
        //
        cout << "Waiting to accept!" << endl;
        tClientSocket = accept(tLocalSocket, NULL, NULL);
        if (INVALID_SOCKET == tClientSocket)
        {
            cout << "=CRITICAL= | accept() call failed. WSAGetLastError=" << WSAGetLastError() << "\n";
            tErr = 1;
            break; // Break out of the for loop
        }

        cout << "Accepted!" << endl;

        //
        // Read data from the incoming stream
        //
        bool bContinue = true;
        tDataBuffer = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, CXN_TRANSFER_DATA_LENGTH);
        if (NULL == tDataBuffer)
        {
            wprintf(L"-FATAL- | HeapAlloc failed | out of memory | gle = [%d] \n", GetLastError());
            tErr = 1;
            break;
        }

        tDataBufferIndex = tDataBuffer;
        unsigned int tTotalLengthReceived = 0;
        int tLengthReceived = 0;
        cout << "Looping to receive!" << endl;
        while (bContinue && (tTotalLengthReceived < CXN_TRANSFER_DATA_LENGTH))
        {
            //
            // recv() call indicates winsock2 to receive data
            // of an expected length over a given connection.
            // recv() may not be able to get the entire length
            // of data at once.  In such case the return value,
            // which specifies the number of bytes received,
            // can be used to calculate how much more data is
            // pending and accordingly recv() can be called again.
            //
            tLengthReceived = recv(tClientSocket, (char *)tDataBufferIndex, (CXN_TRANSFER_DATA_LENGTH - tTotalLengthReceived), 0);

            switch (tLengthReceived) 
            {
                case 0: // socket connection has been closed gracefully
                {
                    bContinue = false;
                    break;
                }
                case SOCKET_ERROR:
                {
                    wprintf(L"=CRITICAL= | recv() call failed. WSAGetLastError=[%d]\n", WSAGetLastError());
                    bContinue = false;
                    tErr = 1;
                    break;
                }
                default:
                {
                    //
                    // Make sure we have enough room
                    //
                    if (tLengthReceived > (CXN_TRANSFER_DATA_LENGTH - tTotalLengthReceived))
                    {
                        wprintf(L"=CRITICAL= | received too much data\n");
                        bContinue = false;
                        tErr = 1;
                        break;
                    }

                    tDataBufferIndex += tLengthReceived;
                    tTotalLengthReceived += tLengthReceived;
                    break;
                }
            }
        }

        if (0 == tErr)
        {
            if (CXN_TRANSFER_DATA_LENGTH != tTotalLengthReceived)
            {
                wprintf(L"+WARNING+ | Data transfer aborted mid-stream. Expected Length = [%I64u], Actual Length = [%d]\n", (ULONG64)CXN_TRANSFER_DATA_LENGTH, tTotalLengthReceived);
            }
            wprintf(L"*INFO* | Received following data string from remote device:\n%s\n", (wchar_t *)tDataBuffer);

            // Close the connection
            if (SOCKET_ERROR == closesocket(tClientSocket))
            {
                wprintf(L"=CRITICAL= | closesocket() call failed w/socket = [0x%I64X]. WSAGetLastError=[%d]\n", (ULONG64)tLocalSocket, WSAGetLastError());
                tErr = 1;
            }
            else
            {
                // Make the connection invalid regardless
                tClientSocket = INVALID_SOCKET;
            }
        }
    }

    if (INVALID_SOCKET != tClientSocket)
    {
        closesocket(tClientSocket);
        tClientSocket = INVALID_SOCKET;
    }

    if (INVALID_SOCKET != tLocalSocket)
    {
        closesocket(tLocalSocket);
        tLocalSocket = INVALID_SOCKET;
    }

    if (NULL != lpCSAddrInfo)
    {
        HeapFree(GetProcessHeap(), 0, lpCSAddrInfo);
        lpCSAddrInfo = NULL;
    }

    if (NULL != tInstanceName)
    {
        HeapFree(GetProcessHeap(), 0, tInstanceName);
        tInstanceName = NULL;
    }

    if (NULL != tDataBuffer)
    {
        HeapFree(GetProcessHeap(), 0, tDataBuffer);
        tDataBuffer = NULL;
    }

    WSACleanup();

    return 0;
}
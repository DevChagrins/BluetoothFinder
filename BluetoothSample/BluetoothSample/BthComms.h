#pragma once

#pragma comment(lib, "Ws2_32.lib")

#include <WinSock2.h>
#include <ws2bth.h>

class BthComms
{
public:
    int RunServer(GUID _GUID, LPWSTR _serviceName);
};
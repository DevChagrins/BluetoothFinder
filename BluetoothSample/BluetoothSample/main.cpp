#include <iostream>
#include <vector>
using namespace std;

#include "BthComms.h"

ULONG NameToBluetoothAddress(const char* _remoteName, vector<PSOCKADDR_BTH>* _socketAddress, vector<LPWSTR>* _serviceName);
const char* ConvertWSTR(LPWSTR _wstr);
LPWSTR CopyWSTR(LPWSTR _wstr);

int main()
{
    cout << "Hello World!" << endl;

    WSADATA btWSAData;
    WORD tVersionRequested;
    int tErr;

    tVersionRequested = MAKEWORD(2, 2);

    tErr = WSAStartup(tVersionRequested, &btWSAData);
    if (tErr != 0)
    {
        cout << "WSAStartup failed with error " << tErr << "\n";
        return 1;
    }

    vector<PSOCKADDR_BTH> tSocketAddresses;
    vector<LPWSTR> tServiceNames;

    NameToBluetoothAddress("Joy-Con", &tSocketAddresses, &tServiceNames);

    GUID SwitchGUID;
    LPCOLESTR guid = L"{E980601A-136E-49A3-B98F-DF5244EB11CC}";
    //LPCOLESTR guid = L"{745a17a0-74d3-11d0-b6fe-00a0c90f57da}";
    CLSIDFromString(guid, &SwitchGUID);

    BthComms TestComms;
    //TestComms.RunServer(tSocketAddresses[0]->serviceClassId, tServiceNames[0]);
    TestComms.RunServer(SwitchGUID, tServiceNames[0]);

    for (int tIndex = 0; tIndex < tSocketAddresses.size(); tIndex++)
    {
        delete tSocketAddresses[tIndex];
    }

    for (int tIndex = 0; tIndex < tServiceNames.size(); tIndex++)
    {
        delete tServiceNames[tIndex];
    }

    return 0;
}

ULONG NameToBluetoothAddress(const char* _remoteName, vector<PSOCKADDR_BTH>* _socketAddress, vector<LPWSTR>* _serviceName)
{
    INT tResult = 0;
    ULONG tFlags = 0, tPQSSize = sizeof(WSAQUERYSET);
    PWSAQUERYSET tWSAQuerySet = NULL;
    PSOCKADDR_BTH tRemoteBtAddr = NULL;

    tWSAQuerySet = (PWSAQUERYSET)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, tPQSSize);

    if (NULL == tWSAQuerySet)
    {
        tResult = STATUS_NO_MEMORY;
        cout << "!ERROR! | Unable to allocate memory for WSAQUERYSET\n";
    }

    if (0 == tResult)
    {
        tFlags = LUP_CONTAINERS;
        tFlags |= LUP_RETURN_NAME;
        tFlags |= LUP_RETURN_ADDR;
        tFlags |= LUP_RETURN_TYPE;

        ULONG tInitialFlags = tFlags;
        tInitialFlags |= LUP_FLUSHCACHE;

        HANDLE tLookupHandle = NULL;

        tResult = 0;
        ZeroMemory(tWSAQuerySet, tPQSSize);
        tWSAQuerySet->dwNameSpace = NS_BTH;
        tWSAQuerySet->dwSize = sizeof(WSAQUERYSET);
        tResult = WSALookupServiceBegin(tWSAQuerySet, tInitialFlags, &tLookupHandle);
        
        if ((tResult == 0) && (tLookupHandle != NULL))
        {
            LPOLESTR bstrGuid = new OLECHAR();
            StringFromCLSID(*tWSAQuerySet->lpServiceClassId, &bstrGuid);
            wcout << "ID: " << bstrGuid << endl;
            if (tWSAQuerySet->lpszServiceInstanceName != NULL)
            {
                const char* tServiceName = ConvertWSTR(tWSAQuerySet->lpszServiceInstanceName);
                if (strstr(tServiceName, _remoteName) != nullptr)
                {
                    tRemoteBtAddr = new SOCKADDR_BTH();
                    ZeroMemory(tRemoteBtAddr, sizeof(*tRemoteBtAddr));
                    CopyMemory(tRemoteBtAddr, (PSOCKADDR_BTH)tWSAQuerySet->lpcsaBuffer->RemoteAddr.lpSockaddr, sizeof(*tRemoteBtAddr));
                    CopyMemory(&tRemoteBtAddr->serviceClassId, tWSAQuerySet->lpServiceClassId, sizeof(GUID));
                    _socketAddress->push_back(tRemoteBtAddr);
                    _serviceName->push_back(CopyWSTR(tWSAQuerySet->lpszServiceInstanceName));
                    cout << "Name: " << tServiceName << endl;
                }

                delete tServiceName;
            }
        }

        bool tContinue = true;
        while (tContinue)
        {
            if (NO_ERROR == WSALookupServiceNext(tLookupHandle, tFlags, &tPQSSize, tWSAQuerySet))
            {
                LPOLESTR bstrGuid = new OLECHAR();
                StringFromCLSID(*tWSAQuerySet->lpServiceClassId, &bstrGuid);
                wcout << "ID: " << bstrGuid << endl;
                if (tWSAQuerySet->lpszServiceInstanceName != NULL)
                {
                    const char* tServiceName = ConvertWSTR(tWSAQuerySet->lpszServiceInstanceName);
                    if (strstr(tServiceName, _remoteName) != nullptr)
                    {
                        tRemoteBtAddr = new SOCKADDR_BTH();
                        ZeroMemory(tRemoteBtAddr, sizeof(*tRemoteBtAddr));
                        CopyMemory(tRemoteBtAddr, (PSOCKADDR_BTH)tWSAQuerySet->lpcsaBuffer->RemoteAddr.lpSockaddr, sizeof(*tRemoteBtAddr));
                        CopyMemory(&tRemoteBtAddr->serviceClassId, tWSAQuerySet->lpServiceClassId, sizeof(GUID));
                        _socketAddress->push_back(tRemoteBtAddr);
                        _serviceName->push_back(CopyWSTR(tWSAQuerySet->lpszServiceInstanceName));
                    }

                    delete tServiceName;
                }
            }
            else
            {
                tResult = WSAGetLastError();
                if (WSA_E_NO_MORE == tResult)
                {
                    cout << "No more service!" << endl;
                    tContinue = false;
                }
                else if (WSAEFAULT == tResult)
                {
                    HeapFree(GetProcessHeap(), 0, tWSAQuerySet);
                    tWSAQuerySet = (PWSAQUERYSET)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, tPQSSize);
                    if (NULL == tWSAQuerySet)
                    {
                        cout << "!ERROR! | Unable to allocate memory for WSAQERYSET\n";
                        tResult = STATUS_NO_MEMORY;
                        tContinue = false;
                    }
                }
                else
                {
                    cout << "=CRITICAL= | WSALookupServiceNext() failed with error code %d\n";
                    tContinue = false;
                }
            }

        }

        WSALookupServiceEnd(tLookupHandle);
    }

    if (NULL != tWSAQuerySet) {
        HeapFree(GetProcessHeap(), 0, tWSAQuerySet);
        tWSAQuerySet = NULL;
    }

    return tResult;
}

const char* ConvertWSTR(LPWSTR _wstr)
{
    size_t tStrLen = wcsnlen(_wstr, 128) + 1;
    char* tStr = new char[tStrLen];
    wcstombs_s(nullptr, tStr, tStrLen, &(*_wstr), tStrLen);

    return tStr;
}

LPWSTR CopyWSTR(LPWSTR _wstr)
{
    size_t tStrLen = wcsnlen(_wstr, 128) + 1;
    LPWSTR tNewStr = new WCHAR[tStrLen];
    ZeroMemory(tNewStr, tStrLen);
    wcsncpy(tNewStr, _wstr, tStrLen);

    return tNewStr;
}
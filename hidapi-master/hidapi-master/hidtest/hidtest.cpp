/*******************************************************
 Windows HID simplification

 Alan Ott
 Signal 11 Software

 8/22/2009

 Copyright 2009
 
 This contents of this file may be used by anyone
 for any reason without any conditions and may be
 used as a starting point for your own applications
 which use HIDAPI.
********************************************************/

#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include "hidapi.h"
#include <iostream>
using namespace std;

#include <vector>;

// Headers needed for sleeping.
#ifdef _WIN32
	#include <windows.h>
#else
	#include <unistd.h>
#endif

int GetTextInput();
int ReadInput(hid_device* _handle, unsigned char* _buffer, int _len);
void PrintOutput(unsigned char* _buffer, int _len);
void BigEndian(unsigned char* _input);

int main(int argc, char* argv[])
{
	int res;
    unsigned char buf[365];
	#define MAX_STR 365
	wchar_t wstr[MAX_STR];
	hid_device *handle;

#ifdef WIN32
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);
#endif

	struct hid_device_info *devs, *cur_dev;
	
	if (hid_init())
		return -1;

	devs = hid_enumerate(0x0, 0x0);
	cur_dev = devs;	
	while (cur_dev) {
		printf("Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls", cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
		printf("\n");
		printf("  Manufacturer: %ls\n", cur_dev->manufacturer_string);
		printf("  Product:      %ls\n", cur_dev->product_string);
		printf("  Release:      %hx\n", cur_dev->release_number);
		printf("  Interface:    %d\n",  cur_dev->interface_number);
		printf("\n");
		cur_dev = cur_dev->next;
	}
	hid_free_enumeration(devs);

	// Set up the command buffer.
	memset(buf,0x00,sizeof(buf));
	buf[0] = 0x01;
	buf[1] = 0x81;
	

	// Open the device using the VID, PID,
	// and optionally the Serial number.
	////handle = hid_open(0x4d8, 0x3f, L"12345");
    //handle = hid_open(0x57e, 0x2007, NULL); // Right Joy-Con
    handle = hid_open(0x57e, 0x2006, NULL); // Left Joy-Con
	if (!handle) {
		printf("unable to open device\n");
 		return 1;
	}

	// Read the Manufacturer String
	wstr[0] = 0x0000;
	res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read manufacturer string\n");
	printf("Manufacturer String: %ls\n", wstr);

	// Read the Product String
	wstr[0] = 0x0000;
	res = hid_get_product_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read product string\n");
	printf("Product String: %ls\n", wstr);

	// Read the Serial Number String
	wstr[0] = 0x0000;
	res = hid_get_serial_number_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read serial number string\n");
	printf("Serial Number String: (%d) %ls", wstr[0], wstr);
	printf("\n");

	// Read Indexed String 1
	wstr[0] = 0x0000;
	res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read indexed string 1\n");
	printf("Indexed String 1: %ls\n", wstr);

	// Set the hid_read() function to be non-blocking.
	hid_set_nonblocking(handle, 1);
	
	// Try to read from the device. There shoud be no
	// data here, but execution should not block.
	res = hid_read(handle, buf, 17);

	// Request state (cmd 0x81). The first byte is the report number (0x1).
    /*for (int i = 0; i < 255; i++)
    {
        memset(buf, 0x00, sizeof(buf));
        res = 0;
        buf[0] = i;
        buf[1] = 0x10;
        res = hid_write(handle, buf, 2);
        if (res < 0)
        {
            printf("Unable to write() (%i): Error: %i\n", i);
            wprintf(L"Error: %s\n", hid_error(handle));
        }
        else
            printf("Index %i: Wrote %i bytes...\n", i, res);
        Sleep(500);
    }*/

 //   memset(buf, 0x00, sizeof(buf));
 //   res = 0;
 //   buf[0] = 0x1;
 //   buf[1] = 0x0;
 //   buf[2] = 0x22;
 //   res = hid_write(handle, buf, 2);
 //   if (res < 0)
 //   {
 //       printf("Unable to write() \n");
 //       wprintf(L"Error: %s\n", hid_error(handle));
 //   }
 //   else
 //       printf("Wrote %i bytes...\n", res);

	//// Read requested state. hid_read() has been set to be
	//// non-blocking by the call to hid_set_nonblocking() above.
	//// This loop demonstrates the non-blocking nature of hid_read().
 //   ReadInput(handle, buf, sizeof(buf));
 //   PrintOutput(buf, sizeof(buf));

    buf[0] = 0x12;
    buf[1] = 0x04;
    buf[2] = 0x04;
    buf[3] = 0x04;
    buf[4] = 0x04;
    res = hid_write(handle, buf, 5);

    vector<int> SpecialFlagValues;
    for (int tValue = 0x0; tValue <= 0x3f; tValue++)
    {
        /*memset(buf, 0x00, sizeof(buf));
        res = 0;
        buf[0] = 0x1;
        buf[1] = tValue;
        res = hid_write(handle, buf, 2);
        if (res < 0)
        {
            printf("Unable to write (%i) \n", tValue);
            wprintf(L"Error: %s\n", hid_error(handle));
        }
        else
        {
            printf("[%i] Wrote %i bytes...\n", tValue, res);
        }*/

        ReadInput(handle, buf, sizeof(buf));
        PrintOutput(buf, sizeof(buf));
        BigEndian(&buf[12]);
    }

    //bool tReadContinue = true;
    //while (tReadContinue)
    //{
    //    memset(buf, 0x00, sizeof(buf));
    //    res = 0;
    //    buf[0] = 0x1;
    //    buf[1] = 0x0;
    //    buf[2] = 0x30;
    //    res = hid_write(handle, buf, 2);
    //    if (res < 0)
    //    {
    //        printf("Unable to write() \n");
    //        wprintf(L"Error: %s\n", hid_error(handle));
    //    }
    //    else
    //        printf("Wrote %i bytes...\n", res);
    //
    //    ReadInput(handle, buf, sizeof(buf));
    //    PrintOutput(buf, sizeof(buf));
    //
    //    //if ((buf[2] & 0x10) || (buf[2] & 0x20))
    //    //    tReadContinue = false;
    //    Sleep(500);
    //}

    printf("Exiting\n");

	hid_close(handle);

	/* Free static HIDAPI objects. */
	hid_exit();

#ifdef WIN32
	system("pause");
#endif

	return 0;
}

int GetTextInput()
{
    char tBuffer[256] = {};
    int tInput = -1;

    cin >> tInput;
    if (cin.bad())
    {
        cin.clear();
        tInput = -1;
    }
    cin.ignore(1i64, '\n');

    return tInput;
}

int ReadInput(hid_device* _handle, unsigned char* _buffer, int _len)
{
    int tResult = 0;
    memset(_buffer, 0x00, _len);

    while (tResult == 0) {
        tResult = hid_read(_handle, _buffer, _len);
        if (tResult < 0)
            printf("Unable to read()\n");
        //else
            //printf("Waiting...\n");
        Sleep(500);
    }
    
    return tResult;
}

void PrintOutput(unsigned char* _buffer, int _len)
{
    if (_buffer != nullptr)
    {
        printf("Data read:\n   ");
        // Print out the returned buffer.
        for (int i = 0; i < _len; i++)
            printf("%02hhx ", _buffer[i]);
        printf("\n");
    }
}

void BigEndian(unsigned char* _input)
{
    int value = 0;
    unsigned char temp[4];
    temp[0] = _input[0];
    temp[1] = _input[1];
    temp[2] = _input[2];
    temp[3] = _input[3];

    memcpy(&value, temp, sizeof(int));
    printf("Value: %i \n", value);
}